package cn.xlbweb.data;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Spring组件初始化类
 *
 * @author: wudibo
 * @since 1.0.0
 */
@Configuration
@Import({EsV1Utils.class, EsV2Utils.class})
public class SpringInit {
}
